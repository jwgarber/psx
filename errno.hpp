#pragma once

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "newtype.hpp"

namespace psx {

    class error_number_tag : comparison {};
    using error_number = newtype<int, error_number_tag>;

    constexpr error_number e2big{E2BIG};
    constexpr error_number eacces{EACCES};
    constexpr error_number eaddrinuse{EADDRINUSE};
    constexpr error_number eaddrnotavail{EADDRNOTAVAIL};
    constexpr error_number eafnosupport{EAFNOSUPPORT};
    constexpr error_number eagain{EAGAIN};
    constexpr error_number ealready{EALREADY};
    constexpr error_number ebadf{EBADF};
    constexpr error_number ebadmsg{EBADMSG};
    constexpr error_number ebusy{EBUSY};
    constexpr error_number ecanceled{ECANCELED};
    constexpr error_number echild{ECHILD};
    constexpr error_number econnaborted{ECONNABORTED};
    constexpr error_number econnrefused{ECONNREFUSED};
    constexpr error_number econnreset{ECONNRESET};
    constexpr error_number edeadlk{EDEADLK};
    constexpr error_number edestaddrreq{EDESTADDRREQ};
    constexpr error_number edom{EDOM};
    constexpr error_number edquot{EDQUOT};
    constexpr error_number eexist{EEXIST};
    constexpr error_number efault{EFAULT};
    constexpr error_number efbig{EFBIG};
    constexpr error_number ehostunreach{EHOSTUNREACH};
    constexpr error_number eidrm{EIDRM};
    constexpr error_number eilseq{EILSEQ};
    constexpr error_number einprogress{EINPROGRESS};
    constexpr error_number eintr{EINTR};
    constexpr error_number einval{EINVAL};
    constexpr error_number eio{EIO};
    constexpr error_number eisconn{EISCONN};
    constexpr error_number eisdir{EISDIR};
    constexpr error_number eloop{ELOOP};
    constexpr error_number emfile{EMFILE};
    constexpr error_number emlink{EMLINK};
    constexpr error_number emsgsize{EMSGSIZE};
    constexpr error_number emultihop{EMULTIHOP};
    constexpr error_number enametoolong{ENAMETOOLONG};
    constexpr error_number enetdown{ENETDOWN};
    constexpr error_number enetreset{ENETRESET};
    constexpr error_number enetunreach{ENETUNREACH};
    constexpr error_number enfile{ENFILE};
    constexpr error_number enobufs{ENOBUFS};
    constexpr error_number enodev{ENODEV};
    constexpr error_number enoent{ENOENT};
    constexpr error_number enolck{ENOLCK};
    constexpr error_number enolink{ENOLINK};
    constexpr error_number enomem{ENOMEM};
    constexpr error_number enomsg{ENOMSG};
    constexpr error_number enoprotoopt{ENOPROTOOPT};
    constexpr error_number enospc{ENOSPC};
    constexpr error_number enosys{ENOSYS};
    constexpr error_number enotconn{ENOTCONN};
    constexpr error_number enotdir{ENOTDIR};
    constexpr error_number enotempty{ENOTEMPTY};
    constexpr error_number enotrecoverable{ENOTRECOVERABLE};
    constexpr error_number enotsock{ENOTSOCK};
    constexpr error_number enotsup{ENOTSUP};
    constexpr error_number enotty{ENOTTY};
    constexpr error_number enxio{ENXIO};
    constexpr error_number eopnotsupp{EOPNOTSUPP};
    constexpr error_number eoverflow{EOVERFLOW};
    constexpr error_number eownerdead{EOWNERDEAD};
    constexpr error_number eperm{EPERM};
    constexpr error_number epipe{EPIPE};
    constexpr error_number eproto{EPROTO};
    constexpr error_number eprotonosupport{EPROTONOSUPPORT};
    constexpr error_number eprototype{EPROTOTYPE};
    constexpr error_number erange{ERANGE};
    constexpr error_number erofs{EROFS};
    constexpr error_number espipe{ESPIPE};
    constexpr error_number esrch{ESRCH};
    constexpr error_number estale{ESTALE};
    constexpr error_number etimedout{ETIMEDOUT};
    constexpr error_number etxtbsy{ETXTBSY};
    constexpr error_number ewouldblock{EWOULDBLOCK};
    constexpr error_number exdev{EXDEV};

    // This is a type that returns a value, or an error_number
    // It is similar to an either, but the second is specialized
    // to be an error_number code
    // T must be some basic datatype (no constructors/destructors) to work in the union
    // Can't use C++17 variant, since that uses exceptions

    [[noreturn]]
    inline void fail(const char* const err_msg) {
        // It's ok if these fail, because we're going to abort anyway
        ::fputs(err_msg, stderr);
        ::fputc('\n', stderr);
        ::exit(EXIT_FAILURE);
    }

    [[noreturn]]
    inline void fail_errno(const char* const err_msg, const error_number err_num) {
        errno = static_cast<int>(err_num);
        ::perror(err_msg);
        ::exit(EXIT_FAILURE);
    }

    template <typename T>
    class [[nodiscard]] result {

        // T cannot have constructors or destructors since it is used in a union.
        //static_assert(std::is_scalar<T>::value);

        private:
        bool success;

        union {
            T value;
            error_number err_num;
        };

        public:

        explicit result(const T value_) : success{true}, value{value_} {}
        explicit result(const error_number err_num_) : success{false}, err_num{err_num_} {}

        T ok(const char* const err_msg) const {

            if (success) {
                return value;
            }

            // Print that we tried to access an error
            fail_errno(err_msg, err_num);
        }

        error_number err() const {

            if (!success) {
                return err_num;
            }

            fail("attempt to access error number of successful result");
        }

        bool is_ok() const {
            return success;
        }

        bool is_err() const {
            return !success;
        }

    };

    template<>
    class [[nodiscard]] result<void> {
        private:
            bool success;
            error_number err_num;

        public:
        // The value doesn't matter, but just init to 0
        explicit result() : success{true}, err_num{0} {}

        explicit result(const error_number err_num_) : success{false}, err_num{err_num_} {}

        void ok(const char* const err_msg) const {

            if (success) {
                return;
            }

            fail_errno(err_msg, err_num);
        }

        error_number err() const {
            if (!success) {
                return err_num;
            }

            fail("attempt to acces error number of successful result");
        }

        bool is_ok() const {
            return success;
        }

        bool is_err() const {
            return !success;
        }
    };
}
