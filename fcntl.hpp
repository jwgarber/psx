#pragma once

#include <fcntl.h>

#include "errno.hpp"
#include "sys/stat.hpp"

namespace psx {

    class open_flag_tag : comparison, bitwise {};
    using open_flag = newtype<int, open_flag_tag>;

    // glibc isn't fully compliant, hence the ifdefs
#ifdef O_EXEC // glibc
    constexpr open_flag o_exec{O_EXEC};
#endif
    constexpr open_flag o_rdonly{O_RDONLY};
    constexpr open_flag o_rdwr{O_RDWR};
#ifdef O_SEARCH // glibc
    constexpr open_flag o_search{O_SEARCH};
#endif
    constexpr open_flag o_wronly{O_WRONLY};
    constexpr open_flag o_append{O_APPEND};
    constexpr open_flag o_cloexec{O_CLOEXEC};
    constexpr open_flag o_creat{O_CREAT};
    constexpr open_flag o_directory{O_DIRECTORY};
    constexpr open_flag o_dsync{O_DSYNC};
    constexpr open_flag o_excl{O_EXCL};
    constexpr open_flag o_noctty{O_NOCTTY};
    constexpr open_flag o_nofollow{O_NOFOLLOW};
    constexpr open_flag o_nonblock{O_NONBLOCK};
    constexpr open_flag o_rsync{O_RSYNC};
    constexpr open_flag o_sync{O_SYNC};
    constexpr open_flag o_trunc{O_TRUNC};
#ifdef O_TTY_INIT // glibc
    constexpr open_flag o_tty_init{O_TTY_INIT};
#endif

    class file_des_tag : comparison {};
    using file_des = newtype<int, file_des_tag>;

    inline result<file_des> open(const char* const path, const open_flag oflag) {

        const auto rval = ::open(path, static_cast<int>(oflag));

        if (rval == -1) {
            const error_number err{errno};
            return result<file_des>{err};
        } else {
            const file_des fd{rval};
            return result<file_des>{fd};
        }
    }

    inline result<file_des> open(const char* const path, const open_flag oflag, const mode mflag) {

        const auto rval = ::open(path, static_cast<int>(oflag), static_cast<mode_t>(mflag));

        if (rval == -1) {
            const error_number err{errno};
            return result<file_des>{err};
        } else {
            const file_des fd{rval};
            return result<file_des>{fd};
        }
    }

    constexpr file_des at_fdcwd{AT_FDCWD};

    // ------------------------------------------------------------------------
    // Advanced Realtime
    // ------------------------------------------------------------------------

    class fadv_tag : comparison {};
    using fadv = newtype<int, fadv_tag>;

    constexpr fadv fadv_normal{POSIX_FADV_NORMAL};
    constexpr fadv fadv_sequential{POSIX_FADV_SEQUENTIAL};
    constexpr fadv fadv_random{POSIX_FADV_RANDOM};
    constexpr fadv fadv_willneed{POSIX_FADV_WILLNEED};
    constexpr fadv fadv_dontneed{POSIX_FADV_DONTNEED};
    constexpr fadv fadv_noreuse{POSIX_FADV_NOREUSE};

    inline result<void> fadvise(const file_des fd, const off_t offset, const off_t len, const fadv advice) {
        const auto rval = ::posix_fadvise(static_cast<int>(fd), offset, len, static_cast<int>(advice));

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{rval};
            return result<void>{err};
        }
    }

    inline result<void> fallocate(const file_des fd, const off_t offset, const off_t len) {

        const auto rval = ::posix_fallocate(static_cast<int>(fd), offset, len);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{rval};
            return result<void>{err};
        }
    }
}
