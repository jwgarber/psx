#pragma once

#include <unistd.h>

#include "errno.hpp"
#include "fcntl.hpp"
#include "sys/types.hpp"

namespace psx {

    // ------------------------------------------------------------------------
    // Sysconf
    // ------------------------------------------------------------------------

    class sysconf_flag_tag : comparison {};
    using sysconf_flag = newtype<int, sysconf_flag_tag>;

    constexpr sysconf_flag sc_aio_listio_max{_SC_AIO_LISTIO_MAX};
    constexpr sysconf_flag sc_aio_max{_SC_AIO_MAX};
    constexpr sysconf_flag sc_aio_prio_delta_max{_SC_AIO_PRIO_DELTA_MAX};
    constexpr sysconf_flag sc_arg_max{_SC_ARG_MAX};
    constexpr sysconf_flag sc_atexit_max{_SC_ATEXIT_MAX};
    constexpr sysconf_flag sc_bc_base_max{_SC_BC_BASE_MAX};
    constexpr sysconf_flag sc_bc_dim_max{_SC_BC_DIM_MAX};
    constexpr sysconf_flag sc_bc_scale_max{_SC_BC_SCALE_MAX};
    constexpr sysconf_flag sc_bc_string_max{_SC_BC_STRING_MAX};
    constexpr sysconf_flag sc_child_max{_SC_CHILD_MAX};
    constexpr sysconf_flag sc_clk_tck{_SC_CLK_TCK};
    constexpr sysconf_flag sc_coll_weights_max{_SC_COLL_WEIGHTS_MAX};
    constexpr sysconf_flag sc_delaytimer_max{_SC_DELAYTIMER_MAX};
    constexpr sysconf_flag sc_expr_nest_max{_SC_EXPR_NEST_MAX};
    constexpr sysconf_flag sc_host_name_max{_SC_HOST_NAME_MAX};
    constexpr sysconf_flag sc_iov_max{_SC_IOV_MAX};
    constexpr sysconf_flag sc_line_max{_SC_LINE_MAX};
    constexpr sysconf_flag sc_login_name_max{_SC_LOGIN_NAME_MAX};
    constexpr sysconf_flag sc_ngroups_max{_SC_NGROUPS_MAX};
    constexpr sysconf_flag sc_getgr_r_size_max{_SC_GETGR_R_SIZE_MAX};
    constexpr sysconf_flag sc_getpw_r_size_max{_SC_GETPW_R_SIZE_MAX};
    constexpr sysconf_flag sc_mq_open_max{_SC_MQ_OPEN_MAX};
    constexpr sysconf_flag sc_mq_prio_max{_SC_MQ_PRIO_MAX};
    constexpr sysconf_flag sc_open_max{_SC_OPEN_MAX};
    constexpr sysconf_flag sc_page_size{_SC_PAGE_SIZE};
    constexpr sysconf_flag sc_pagesize{_SC_PAGESIZE};
    constexpr sysconf_flag sc_thread_destructor_iterations{_SC_THREAD_DESTRUCTOR_ITERATIONS};
    constexpr sysconf_flag sc_thread_keys_max{_SC_THREAD_KEYS_MAX};
    constexpr sysconf_flag sc_thread_stack_min{_SC_THREAD_STACK_MIN};
    constexpr sysconf_flag sc_thread_threads_max{_SC_THREAD_THREADS_MAX};
    constexpr sysconf_flag sc_re_dup_max{_SC_RE_DUP_MAX};
    constexpr sysconf_flag sc_rtsig_max{_SC_RTSIG_MAX};
    constexpr sysconf_flag sc_sem_nsems_max{_SC_SEM_NSEMS_MAX};
    constexpr sysconf_flag sc_sem_value_max{_SC_SEM_VALUE_MAX};
    constexpr sysconf_flag sc_sigqueue_max{_SC_SIGQUEUE_MAX};
    constexpr sysconf_flag sc_stream_max{_SC_STREAM_MAX};
    constexpr sysconf_flag sc_symloop_max{_SC_SYMLOOP_MAX};
    constexpr sysconf_flag sc_timer_max{_SC_TIMER_MAX};
    constexpr sysconf_flag sc_tty_name_max{_SC_TTY_NAME_MAX};
    constexpr sysconf_flag sc_tzname_max{_SC_TZNAME_MAX};
    constexpr sysconf_flag sc_advisory_info{_SC_ADVISORY_INFO};
    constexpr sysconf_flag sc_barriers{_SC_BARRIERS};
    constexpr sysconf_flag sc_asynchronous_io{_SC_ASYNCHRONOUS_IO};
    constexpr sysconf_flag sc_clock_selection{_SC_CLOCK_SELECTION};
    constexpr sysconf_flag sc_cputime{_SC_CPUTIME};
    constexpr sysconf_flag sc_fsync{_SC_FSYNC};
    constexpr sysconf_flag sc_ipv6{_SC_IPV6};
    constexpr sysconf_flag sc_job_control{_SC_JOB_CONTROL};
    constexpr sysconf_flag sc_mapped_files{_SC_MAPPED_FILES};
    constexpr sysconf_flag sc_memlock{_SC_MEMLOCK};
    constexpr sysconf_flag sc_memlock_range{_SC_MEMLOCK_RANGE};
    constexpr sysconf_flag sc_memory_protection{_SC_MEMORY_PROTECTION};
    constexpr sysconf_flag sc_message_passing{_SC_MESSAGE_PASSING};
    constexpr sysconf_flag sc_monotonic_clock{_SC_MONOTONIC_CLOCK};
    constexpr sysconf_flag sc_prioritized_io{_SC_PRIORITIZED_IO};
    constexpr sysconf_flag sc_priority_scheduling{_SC_PRIORITY_SCHEDULING};
    constexpr sysconf_flag sc_raw_sockets{_SC_RAW_SOCKETS};
    constexpr sysconf_flag sc_reader_writer_locks{_SC_READER_WRITER_LOCKS};
    constexpr sysconf_flag sc_realtime_signals{_SC_REALTIME_SIGNALS};
    constexpr sysconf_flag sc_regexp{_SC_REGEXP};
    constexpr sysconf_flag sc_saved_ids{_SC_SAVED_IDS};
    constexpr sysconf_flag sc_semaphores{_SC_SEMAPHORES};
    constexpr sysconf_flag sc_shared_memory_objects{_SC_SHARED_MEMORY_OBJECTS};
    constexpr sysconf_flag sc_shell{_SC_SHELL};
    constexpr sysconf_flag sc_spawn{_SC_SPAWN};
    constexpr sysconf_flag sc_spin_locks{_SC_SPIN_LOCKS};
    constexpr sysconf_flag sc_sporadic_server{_SC_SPORADIC_SERVER};
    constexpr sysconf_flag sc_ss_repl_max{_SC_SS_REPL_MAX};
    constexpr sysconf_flag sc_synchronized_io{_SC_SYNCHRONIZED_IO};
    constexpr sysconf_flag sc_thread_attr_stackaddr{_SC_THREAD_ATTR_STACKADDR};
    constexpr sysconf_flag sc_thread_attr_stacksize{_SC_THREAD_ATTR_STACKSIZE};
    constexpr sysconf_flag sc_thread_cputime{_SC_THREAD_CPUTIME};
    constexpr sysconf_flag sc_thread_prio_inherit{_SC_THREAD_PRIO_INHERIT};
    constexpr sysconf_flag sc_thread_prio_protect{_SC_THREAD_PRIO_PROTECT};
    constexpr sysconf_flag sc_thread_priority_scheduling{_SC_THREAD_PRIORITY_SCHEDULING};
    constexpr sysconf_flag sc_thread_process_shared{_SC_THREAD_PROCESS_SHARED};
    constexpr sysconf_flag sc_thread_robust_prio_inherit{_SC_THREAD_ROBUST_PRIO_INHERIT};
    constexpr sysconf_flag sc_thread_robust_prio_protect{_SC_THREAD_ROBUST_PRIO_PROTECT};
    constexpr sysconf_flag sc_thread_safe_functions{_SC_THREAD_SAFE_FUNCTIONS};
    constexpr sysconf_flag sc_thread_sporadic_server{_SC_THREAD_SPORADIC_SERVER};
    constexpr sysconf_flag sc_threads{_SC_THREADS};
    constexpr sysconf_flag sc_timeouts{_SC_TIMEOUTS};
    constexpr sysconf_flag sc_timers{_SC_TIMERS};
    constexpr sysconf_flag sc_trace{_SC_TRACE};
    constexpr sysconf_flag sc_trace_event_filter{_SC_TRACE_EVENT_FILTER};
    constexpr sysconf_flag sc_trace_event_name_max{_SC_TRACE_EVENT_NAME_MAX};
    constexpr sysconf_flag sc_trace_inherit{_SC_TRACE_INHERIT};
    constexpr sysconf_flag sc_trace_log{_SC_TRACE_LOG};
    constexpr sysconf_flag sc_trace_name_max{_SC_TRACE_NAME_MAX};
    constexpr sysconf_flag sc_trace_sys_max{_SC_TRACE_SYS_MAX};
    constexpr sysconf_flag sc_trace_user_event_max{_SC_TRACE_USER_EVENT_MAX};
    constexpr sysconf_flag sc_typed_memory_objects{_SC_TYPED_MEMORY_OBJECTS};
    constexpr sysconf_flag sc_version{_SC_VERSION};
    constexpr sysconf_flag sc_v7_ilp32_off32{_SC_V7_ILP32_OFF32};
    constexpr sysconf_flag sc_v7_ilp32_offbig{_SC_V7_ILP32_OFFBIG};
    constexpr sysconf_flag sc_v7_lp64_off64{_SC_V7_LP64_OFF64};
    constexpr sysconf_flag sc_v7_lpbig_offbig{_SC_V7_LPBIG_OFFBIG};
    constexpr sysconf_flag sc_2_c_bind{_SC_2_C_BIND};
    constexpr sysconf_flag sc_2_c_dev{_SC_2_C_DEV};
    constexpr sysconf_flag sc_2_char_term{_SC_2_CHAR_TERM};
    constexpr sysconf_flag sc_2_fort_dev{_SC_2_FORT_DEV};
    constexpr sysconf_flag sc_2_fort_run{_SC_2_FORT_RUN};
    constexpr sysconf_flag sc_2_localedef{_SC_2_LOCALEDEF};
    constexpr sysconf_flag sc_2_pbs{_SC_2_PBS};
    constexpr sysconf_flag sc_2_pbs_accounting{_SC_2_PBS_ACCOUNTING};
    constexpr sysconf_flag sc_2_pbs_checkpoint{_SC_2_PBS_CHECKPOINT};
    constexpr sysconf_flag sc_2_pbs_locate{_SC_2_PBS_LOCATE};
    constexpr sysconf_flag sc_2_pbs_message{_SC_2_PBS_MESSAGE};
    constexpr sysconf_flag sc_2_pbs_track{_SC_2_PBS_TRACK};
    constexpr sysconf_flag sc_2_sw_dev{_SC_2_SW_DEV};
    constexpr sysconf_flag sc_2_upe{_SC_2_UPE};
    constexpr sysconf_flag sc_2_version{_SC_2_VERSION};
    constexpr sysconf_flag sc_xopen_crypt{_SC_XOPEN_CRYPT};
    constexpr sysconf_flag sc_xopen_enh_i18n{_SC_XOPEN_ENH_I18N};
    constexpr sysconf_flag sc_xopen_realtime{_SC_XOPEN_REALTIME};
    constexpr sysconf_flag sc_xopen_realtime_threads{_SC_XOPEN_REALTIME_THREADS};
    constexpr sysconf_flag sc_xopen_shm{_SC_XOPEN_SHM};
    constexpr sysconf_flag sc_xopen_streams{_SC_XOPEN_STREAMS};
    constexpr sysconf_flag sc_xopen_unix{_SC_XOPEN_UNIX};
#ifdef _SC_XOPEN_UUCP
    constexpr sysconf_flag sc_xopen_uucp{_SC_XOPEN_UUCP}; // glibc
#endif
    constexpr sysconf_flag sc_xopen_version{_SC_XOPEN_VERSION};

    inline result<long> sysconf(const sysconf_flag sc) {
        errno = 0;
        const auto rval = ::sysconf(static_cast<int>(sc));

        if (rval == -1 && errno != 0) {
            const error_number err{errno};
            return result<long>{err};
        } else {
            return result<long>{rval};
        }
    }

#if 0

    enum class cs : int {
        path = _CS_PATH,
        posix_v7_ilp32_off32_cflags = _CS_POSIX_V7_ILP32_OFF32_CFLAGS,
        posix_v7_ilp32_off32_ldflags = _CS_POSIX_V7_ILP32_OFF32_LDFLAGS,
        posix_v7_ilp32_off32_libs = _CS_POSIX_V7_ILP32_OFF32_LIBS,
        posix_v7_ilp32_offbig_cflags = _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS,
        posix_v7_ilp32_offbig_ldflags = _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS,
        posix_v7_ilp32_offbig_libs = _CS_POSIX_V7_ILP32_OFFBIG_LIBS,
        posix_v7_lp64_off64_cflags = _CS_POSIX_V7_LP64_OFF64_CFLAGS,
        posix_v7_lp64_off64_ldflags = _CS_POSIX_V7_LP64_OFF64_LDFLAGS,
        posix_v7_lp64_off64_libs = _CS_POSIX_V7_LP64_OFF64_LIBS,
        posix_v7_lpbig_offbig_cflags = _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS,
        posix_v7_lpbig_offbig_ldflags = _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS,
        posix_v7_lpbig_offbig_libs = _CS_POSIX_V7_LPBIG_OFFBIG_LIBS,
        posix_v7_threads_cflags = _CS_POSIX_V7_THREADS_CFLAGS,
        posix_v7_threads_ldflags = _CS_POSIX_V7_THREADS_LDFLAGS,
        eosix_v7_width_restricted_envs = _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS,
        v7_env = _CS_V7_ENV,
    };

    result<size_t> confstr(const cs name, char* buf, const size_t len) {

        errno = 0;
        const auto size = ::confstr(static_cast<int>(name), buf, len);

        if (size == 0 && errno != 0) {
            const auto err = convert_errno(errno);
            return result<size_t>{err};
        } else {
            return result<size_t>{size};
        }
    }
#endif

    constexpr file_des stdin_fileno{STDIN_FILENO};
    constexpr file_des stdout_fileno{STDOUT_FILENO};
    constexpr file_des stderr_fileno{STDERR_FILENO};

    // ------------------------------------------------------------------------
    // I/O
    // ------------------------------------------------------------------------

    inline result<ssize_t> read(const file_des fd, unsigned char* const buf, const size_t nbyte) {

        const auto bytes_read = ::read(static_cast<int>(fd), buf, nbyte);
        if (bytes_read == -1) {
            const error_number err{errno};
            return result<ssize_t>{err};
        } else {
            return result<ssize_t>{bytes_read};
        }
    }

    inline result<ssize_t> write(const file_des fd, const unsigned char* const buf, const size_t nbyte) {

        const auto bytes_written = ::write(static_cast<int>(fd), buf, nbyte);
        if (bytes_written == -1) {
            const error_number err{errno};
            return result<ssize_t>{err};
        } else {
            return result<ssize_t>{bytes_written};
        }
    }

    inline result<void> close(const file_des fd) {
        const auto status = ::close(static_cast<int>(fd));
        if (status == -1) {
            const error_number err{errno};
            return result<void>{err};
        } else {
            return result<void>{};
        }
    }

    class seek_flag_tag : comparison {};
    using seek_flag = newtype<int, seek_flag_tag>;

    constexpr seek_flag seek_set{SEEK_SET};
    constexpr seek_flag seek_cur{SEEK_CUR};
    constexpr seek_flag seek_end{SEEK_END};

    inline result<off_t> lseek(const file_des fd, const off_t offset, const seek_flag whence) {
        const auto rval = ::lseek(static_cast<int>(fd), offset, static_cast<int>(whence));
        if (rval == -1) {
            const error_number err{errno};
            return result<off_t>{err};
        } else {
            return result<off_t>{rval};
        }
    }

    inline result<void> truncate(const char* const path, const off_t length) {

        const auto rval = ::truncate(path, length);
        if (rval == -1) {
            const error_number err{errno};
            return result<void>{err};
        } else {
            return result<void>{};
        }
    }

    inline result<void> ftruncate(const file_des fd, const off_t length) {
        const auto rval = ::ftruncate(static_cast<int>(fd), length);
        if (rval == -1) {
            const error_number err{errno};
            return result<void>{err};
        } else {
            return result<void>{};
        }
    }

    inline result<char*> getcwd(char* const buf, const size_t len) {

        const auto rval = ::getcwd(buf, len);

        if (rval == nullptr) {
            const error_number err{errno};
            return result<char*>{err};
        } else {
            return result<char*>{rval};
        }
    }

    inline result<void> chdir(const char* const path) {

        const auto rval = ::chdir(path);

        if (rval == -1) {
            const error_number err{errno};
            return result<void>{err};
        } else {
            return result<void>{};
        }
    }

    inline result<void> fchdir(const file_des fd) {

        const auto rval = ::fchdir(static_cast<int>(fd));

        if (rval == -1) {
            const error_number err{errno};
            return result<void>{err};
        } else {
            return result<void>{};
        }
    }

    inline result<pid> fork() {

        const auto rval = ::fork();

        if (rval == -1) {
            const error_number err{errno};
            return result<pid>{err};
        } else {
            return result<pid>{pid{rval}};
        }
    }

    // ------------------------------------------------------------------------
    // PID
    // ------------------------------------------------------------------------

    inline pid getpid() {
        const auto p = ::getpid();
        return pid{p};
    }

    inline pid getppid() {
        const auto p = ::getppid();
        return pid{p};
    }

    // TODO this also needs the casting rules I think
    //inline result<void> pipe() {}
}
