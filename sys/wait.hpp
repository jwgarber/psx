#pragma once

#include <sys/wait.h>

#include "../errno.hpp"
#include "../newtype.hpp"
#include "../opt_ref.hpp"

namespace psx {

    class wait_status_tag : comparison {};
    using wait_status = newtype<int, wait_status_tag>;

    class wait_option_tag : comparison, bitwise {};
    using wait_option = newtype<int, wait_option_tag>;

    // What about a bitmask of nothing? Would we just do wait_option{0}?
    // Seems reasonable to me.

    // Options for waitpid and waitid. Not all of these are valid for both
    // functions, but they share some, so it makes sense to merge them.
    constexpr wait_option wcontinued{WCONTINUED};
    constexpr wait_option wnohang{WNOHANG};
    constexpr wait_option wuntraced{WUNTRACED};

    constexpr wait_option wexited{WEXITED};
    constexpr wait_option wnowait{WNOWAIT};
    constexpr wait_option wstopped{WSTOPPED};

    inline result<pid> wait(const opt_ref<wait_status> status) {

        // TODO we need to reinterpret_cast this I think

        // Slightly more complicated than I would like, but such is life
        auto st = static_cast<wait_status*>(status);
        pid_t rval;
        if (st == nullptr) {
            rval = ::wait(nullptr);
        } else {
            int stat;
            rval = ::wait(&stat);

            if (rval != -1) {
                *st = wait_status{stat};
            }
        }

        if (rval == -1) {
            const error_number err{errno};
            return result<pid>{err};
        } else {
            return result<pid>{pid{rval}};
        }
    }

    inline bool wifexited(const wait_status status) {
        return WIFEXITED(static_cast<int>(status));
    }

    inline bool wifsignaled(const wait_status status) {
        return WIFSIGNALED(static_cast<int>(status));
    }

    inline bool wifstopped(const wait_status status) {
        return WIFSTOPPED(static_cast<int>(status));
    }

    inline bool wifcontinued(const wait_status status) {
        return WIFCONTINUED(static_cast<int>(status));
    }
}

