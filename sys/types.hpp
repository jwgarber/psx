#pragma once

#include <sys/types.h>

#include "../newtype.hpp"

namespace psx {

    class mode_tag : comparison, bitwise {};
    using mode = newtype<mode_t, mode_tag>;

    class pid_tag : comparison {};
    using pid = newtype<pid_t, pid_tag>;
}
