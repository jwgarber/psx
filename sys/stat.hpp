#pragma once

#include <sys/stat.h>

#include "types.hpp"
#include "../errno.hpp"

namespace psx {

    constexpr mode s_irwxu{S_IRWXU};
    constexpr mode s_irusr{S_IRUSR};
    constexpr mode s_iwusr{S_IWUSR};
    constexpr mode s_ixusr{S_IXUSR};
    constexpr mode s_irwxg{S_IRWXG};
    constexpr mode s_irgrp{S_IRGRP};
    constexpr mode s_iwgrp{S_IWGRP};
    constexpr mode s_ixgrp{S_IXGRP};
    constexpr mode s_irwxo{S_IRWXO};
    constexpr mode s_iroth{S_IROTH};
    constexpr mode s_iwoth{S_IWOTH};
    constexpr mode s_ixoth{S_IXOTH};
    constexpr mode s_isuid{S_ISUID};
    constexpr mode s_isgid{S_ISGID};
    constexpr mode s_isvtx{S_ISVTX};

    inline mode umask(const mode mask) {
        const auto rval = ::umask(static_cast<mode_t>(mask));
        return mode{rval};
    }

    inline result<void> mkfifo(const char* const pathname, const mode mask) {

        const auto rval = ::mkfifo(pathname, static_cast<mode_t>(mask));

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }
}
