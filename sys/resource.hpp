#pragma once

#include <sys/resource.h>

#include "../errno.hpp"

namespace psx {

    class usage_proc_tag : comparison {};
    using usage_proc = newtype<int, usage_proc_tag>;

    constexpr usage_proc rusage_self{RUSAGE_SELF};
    constexpr usage_proc rusage_children{RUSAGE_CHILDREN};

    struct rusage : public ::rusage {
        // This constructor will default initialize all fields
        explicit rusage() : ::rusage{} {}
    };

    inline result<void> getrusage(const usage_proc who, rusage& usage) {

        const auto rval = getrusage(static_cast<int>(who), &usage);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }

    class resource_tag : comparison {};
    using resource = newtype<int, resource_tag>;

    constexpr resource rlimit_as{RLIMIT_AS};
    constexpr resource rlimit_core{RLIMIT_CORE};
    constexpr resource rlimit_cpu{RLIMIT_CPU};
    constexpr resource rlimit_data{RLIMIT_DATA};
    constexpr resource rlimit_fsize{RLIMIT_FSIZE};
    constexpr resource rlimit_nofile{RLIMIT_NOFILE};
    constexpr resource rlimit_stack{RLIMIT_STACK};

    constexpr rlim_t rlim_infinity{RLIM_INFINITY};

    constexpr rlim_t rlim_saved_cur{RLIM_SAVED_CUR};
    constexpr rlim_t rlim_saved_max{RLIM_SAVED_MAX};

    struct rlimit : public ::rlimit {
        explicit rlimit(const rlim_t rlim_cur_, const rlim_t rlim_max_) : ::rlimit{} {
            rlim_cur = rlim_cur_;
            rlim_max = rlim_max_;
        }
    };

    inline result<void> getrlimit(const resource res, rlimit& rlim) {

        const auto rval = ::getrlimit(static_cast<int>(res), &rlim);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }

    inline result<void> setrlimit(const resource res, const rlimit& rlim) {

        const auto rval = ::setrlimit(static_cast<int>(res), &rlim);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }
}
