#pragma once

#include <type_traits>

namespace psx {

    struct comparison {};

    struct bitwise {};

    template<typename T, typename Tag>
    class newtype {

        // Might need this for pointer finangling
        //static_assert(sizeof(T) == sizeof(newtype<T, Tag>));

        private:
        T value;

        public:

        // For some types, it is nice to have a default initialization value (like for wait_status),
        // because it will be overwritten soon.
        // In these cases, we simply default initialize the value.
        constexpr explicit newtype() : value{} {}

        constexpr explicit newtype(const T value_) : value{value_} {}

        constexpr explicit operator T() const { return value; }

        // Static asserts are nice, because that way you can write a helpful
        // error message instead of getting blasted with template madness.
        // Maybe concepts will save us all.
        constexpr void operator&=(const newtype<T, Tag> rhs) {
            static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
            value &= rhs.value;
        }

        constexpr void operator|=(const newtype<T, Tag> rhs) {
            static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
            value |= rhs.value;
        }

        constexpr void operator^=(const newtype<T, Tag> rhs) {
            static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
            value ^= rhs.value;
        }
    };

    template<typename T, typename Tag>
    constexpr bool operator==(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) == static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr bool operator!=(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) != static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr bool operator<(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) < static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr bool operator>(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) > static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr bool operator<=(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) <= static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr bool operator>=(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<comparison, Tag>::value, "This type does not support comparison operators");
        return static_cast<T>(lhs) >= static_cast<T>(rhs);
    }

    template<typename T, typename Tag>
    constexpr newtype<T, Tag> operator&(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
        const auto val = static_cast<T>(lhs) & static_cast<T>(rhs);
        return newtype<T, Tag>{val};
    }

    template<typename T, typename Tag>
    constexpr newtype<T, Tag> operator|(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
        const auto val = static_cast<T>(lhs) | static_cast<T>(rhs);
        return newtype<T, Tag>{val};
    }

    template<typename T, typename Tag>
    constexpr newtype<T, Tag> operator^(const newtype<T, Tag> lhs, const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
        const auto val = static_cast<T>(lhs) ^ static_cast<T>(rhs);
        return newtype<T, Tag>{val};
    }

    template<typename T, typename Tag>
    constexpr newtype<T, Tag> operator~(const newtype<T, Tag> rhs) {
        static_assert(std::is_base_of<bitwise, Tag>::value, "This type does not support bitwise operators");
        const auto val = ~static_cast<T>(rhs);
        return newtype<T, Tag>{val};
    }
}
