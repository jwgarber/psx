#pragma once

#include <time.h>

#include "result.hpp"

namespace psx {

    struct timespec : public ::timespec {
        explicit timespec(const time_t tv_sec_, const long tv_nsec_) {
            tv_sec = tv_sec_;
            tv_nsec = tv_nsec_;
        }
    };

    struct itimerspec : public ::itimerspec {
        explicit itimerspec(const timespec it_interval_, const timespec it_value_) {
            it_interval = it_interval_;
            it_value = it_value_;
        }
    };

    class clockid_tag : comparison {};
    using clockid = newtype<clockid, clockid_tag>;

    constexpr clockid clock_realtime{CLOCK_REALTIME};
    constexpr clockid clock_monotonic{CLOCK_MONOTONIC};
    constexpr clockid clock_process_cputime_id{CLOCK_PROCESS_CPUTIME_ID};
    constexpr clockid clock_thread_cputime_id{CLOCK_THREAD_CPUTIME_ID};

    inline result<void> clock_getres(const clockid clock_id, timespec& res) {

        const auto rval = ::clock_getres(static_cast<clockid_t>(clock_id), &res);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }

    inline result<void> clock_gettime(const clockid clock_id, timespec& tp) {

        const auto rval = ::clock_gettime(static_cast<clockid_t>(clock_id), &tp);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }

    inline result<void> clock_settime(const clockid clock_id, timespec& tp) {

        const auto rval = ::clock_settime(static_cast<clockid_t>(clock_id), &tp);

        if (rval == 0) {
            return result<void>{};
        } else {
            const error_number err{errno};
            return result<void>{err};
        }
    }

    // Time
    // Storing a return value is silly
    // Man page says it is deprecated anyway
    // We will just return the result
    // TODO remove this? Maybe, maybe not.
    // It might be nice to include things that round out the standard
    result<time_t> time() {
        const auto t = ::time(nullptr);
        if (t == static_cast<time_t>(-1)) {
            const auto err = convert_errno(errno);
            return result<time_t>{err};
        } else {
            return result<time_t>{t};
        }
    }
}
