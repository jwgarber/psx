#pragma once

//#include <cstddef>

namespace psx {

    // Similar to std::reference_wrapper
    // Perhaps do an assert here too
    template <typename T>
    class opt_ref {
        private:
            T* ptr;

        public:
            // These constructors are NOT explicit to simplify the API. Will this cause problems?
            // Right now we use {} instead of nullptr. I rather like that
            constexpr opt_ref() : ptr{nullptr} {}

            constexpr opt_ref(T& ref) : ptr{&ref} {}

        constexpr explicit operator T*() const { return ptr; }

            // TODO perhaps delete some operators here
    };
}

